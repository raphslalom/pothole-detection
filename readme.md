# Pothole detection



## Pot hole datasets:
https://www.kaggle.com/hiteshsankhat/pothole-dataset#new_model.zip
https://www.researchgate.net/publication/282807920_Dataset_of_images_used_for_pothole_detection
https://drive.google.com/drive/folders/1vUmCvdW3-2lMrhsMbXdMWeLcEz__Ocuy
 
## traffic lights:
https://www.kaggle.com/mbornoe/lisa-traffic-light-dataset
 
 
## Google maps API streetview static API
https://developers.google.com/maps/documentation/streetview/intro?hl=en
https://developers.google.com/maps/documentation/streetview/usage-and-billing
 